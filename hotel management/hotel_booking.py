from openerp.osv import osv,fields
from datetime import datetime

class dinner_booking(osv.osv):
	_name="dinner.booking"
	_columns={
		
		'name':fields.char('Name',size=64,required=True),
		'members':fields.integer('Member'),
		'location': fields.selection([('hyderabad','Hyderabad'),
			('bangalore','Bangalore')],'Location',required=True),
		'user_id': fields.many2one('res.users', 'Users'),
		'from_datetime': fields.datetime('From Datetime'),
		'to_datetime': fields.datetime('To Datetime'),
		# 'color':fields.char(string="Color",help="Choose your color",size=7),
	}
class room_booking(osv.osv):
	_name="room.booking"
	_columns={
		
		'name':fields.char('Name',size=64),
		'type':fields.char('Type'),
		"availability": fields.boolean("Availability"),
		'created_datetime':fields.datetime('Created At'),
		'updated_datetime':fields.datetime('Updated At'),
	}

	def create(self, cr, uid, vals, context=None):
		vals['created_datetime'] = datetime.now()
		return super(room_booking, self).create(cr, uid, vals, context=context)
			
# class res_partner(osv.osv):
# 	_inherit ="res.partner"
# 	_columns={

# 		'instructor':fields.Boolean("Instructor", default=False)
# 	}