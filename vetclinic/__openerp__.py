{
	'name':'Vet Clinic',
	'version':'1.0',
	'description':"""
	   Vet clinic application
	    -List of animals
	    -list of breeds
	    -create appointment
	  """,
	 'author':'AJAY',
	 'depends':['base'],
	 'data':['vetclinic_view.xml',
	 		'wizards_view.xml',
	 		'sequence_view.xml',
        	'report_studentattendancelist.xml',
        	'student_attendance_report.xml'],
	 'demo':[],
	 'installable':True,
	 'auto_install':False,
	 'qweb': [],
}